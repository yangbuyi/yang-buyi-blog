---
id: yby
title: 杨不易呀
description: 欢迎来到我的个人博客
---
import BVideo from "../../src/components/BVideo";

### 🍺欢迎访问杨不易呀博客！

用来记录自己的笔记和错误等技术问题!!!!

<blockquote class="blockquote"><p>
    <span class='box1'>基本信息 </span>
    <br/>
      个人信息：男 / 汉 / 00后 <br/>
      职业技能： Java / Linux / Web <br/>
    <p>
    <span class='box1'>个人说明 </span>
    <br/>
     后端工程师，酷爱美女
    </p>
    <p>
    <span class='box1'>联系方式 </span>
    <br/>
      E-Mail：yangs@zjc.com <br/>
      QQ：1692700664 <br/>
      QQEmail：1692700664@qq.com <br/>
      WeChat：yangbuyiya <br/>
    </p>
</p></blockquote>

`🍺你的压力源于无法自律，只是假装努力，现状跟不上你内心的欲望，所以你焦急又恐慌---杨不易🍺.|`



<blockquote class="blockquote">分享专栏</blockquote>


## 价值超高的教程

#### 🍺基于SpringCloudAlibaba货币交易系统_大型_项目_武汉尚学堂
<BVideo src="//player.bilibili.com/player.html?aid=372879718&bvid=BV1gZ4y1G7Kf&cid=263857263&page=8" bsrc="https://www.bilibili.com/video/BV1gZ4y1G7Kf?p=8"/>


## 分享我爱听的音乐吧 PGOne

#### 🍺PG ONE突然Diss舆论！发出狠货《Talk Too Much》
<BVideo src="//player.bilibili.com/player.html?aid=927419945&bvid=BV1MT4y1w78Y&cid=244192106&page=1" bsrc="https://www.bilibili.com/video/BV1Gv411k7cn"/>

#### 🍺PG ONE发布新曲，疯狂Diss自己进行忏悔？《KILL THE ONE》
<BVideo src="//player.bilibili.com/player.html?aid=414460200&bvid=BV1DV41127tx&cid=231314062&page=1" bsrc="https://www.bilibili.com/video/BV1DV41127tx"/>

